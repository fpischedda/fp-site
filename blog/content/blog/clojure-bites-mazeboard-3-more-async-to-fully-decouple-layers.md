+++
title = "Clojure Bites - Mazeboard 3 - core.async to update the UI layer"
author = "FPSD"
date = 2023-12-09
tags = "clojure, clojurescript, dumdom, event handler, data driven frontend, core.async, core.match"
+++

# Intro

In the last [post](@/blog/clojure-bites-mazeboard-2-core-async-to-separate-game-ui-logic.md)
we have started decoupling game and UI logic in order to be able
to run the two layers in different processes; we are not there
yet and it will take some time to get to the final client/server
setup, but we will get there, eventually :)

We have moved some logic and the shared state to the game layer,
even if the UI is still using the state atom from the `mazeboard.game`
namespace, actions are sent to game layer using `core.async` instead
of calling the update functions directly; it is not much but it is
honest work.

Goal of this post is to further decouple UI and game logic by having
a separate state for both layers and propagate changes from the game
layer to the UI via `core.async`.

# Recap of the previous changes

We started from something like this:

- UI is rendered using dumdom components driven by a map describing the game state
- DOM events like clicks are handled by dumdom's global event handler, again data driven
- data associated with events describe actions that the player wants to do like moving in the board
- actions are handled by directly calling the game layer which updates the global state
- the UI is subscribed to the global state and re-renders when the state atom changes

And we ended up with the new setup:

- the shared have been moved from `maeboard.ui.core` to `mazeboard.game` namespace
- actions are sent to the game layer using a `core.async` channel which the game layer listens to, to break the direct call dependency

The UI still listen to the game state atom directly, and we are going to fix this.

The commit is available [here](https://gitlab.com/fpischedda/mazeboard/-/commit/11de4bafb1a7ea65addfafbf957af10febc9f8f1).

# Two layers, two state atoms

We want to use two distinct state atoms for:

- managing and mutating the game state according to requested actions
- update the UI state that drives the rendering of the game board

The most obvious thing to do would be to keep the pre existing game
state atom in the `mazeboard.game` namespace and a dedicated state 
atom to `mazeboard.ui.core` namespace so that the two layers can 
happily live their independent lives :) 

This is not enough, because we want to be able to update the UI state
after something happened in the game layer; this must somehow be communicated
to the UI layer via `core.async` channels but given that now we are directly
"assoc"ing or "update(-in)"ing directly to the game state map, it is not easy
to transfer the intended changes between layers.

Let's do a step back, or sideways, to try do define a data format that 
can describe state changes that can be transferred between layers and
applied accordingly to both states.

# Abstracting state changes

We can start by having a look at how the action that handles player's 
movement is implemented:

```clojure

(defn action-move-player
  [{:keys [current-player players] :as game} {:keys [direction target]}]
  (let [old-pos (get-in players [target :position])
        new-pos (update-position (get-in game [:players target :position]) direction)]
    (-> game
        (assoc-in [:players target :position] new-pos)
        (assoc :current-player (mod (inc current-player) (count players)))
        (update-in [:board :tiles old-pos :players] disj target)
        (update-in [:board :tiles new-pos :players] conj target)
        (update :round-number inc))))

```

All it is doing is to take the game map, and applying `assoc(-in)` and 
`update(-in)` functions to it; `update` and `update-in` can be replaced by
their assoc forms if handled correctly so, if we can replace the direct calls
to assoc(-in) functions to data then we then can abstract away what we want to do
using just data in a form the looks something like:

```clojure

[[:assoc-in [:players target :position] new-pos]
 [:assoc-in [:board :tiles old-pos :players] old-tile-players]
 [:assoc-in [:board :tiles new-pos :players] new-tile-players]
 [:assoc :current-player (mod (inc current-player) (count players))]
 [:assoc :round-number (inc round-number)]]

```

We can then have a function that takes a data structure, the requested
changes and that can spit out a new, updated data structure:

```clojure

(defn apply-commands
  [state commands]
  (reduce
   (fn [game command]
     (match command
            [:assoc key value] (assoc game key value)
            [:assoc-in path value] (assoc-in game path value)))
    state commands))

```

With all these building blocks in place what we have left to do is to
update our code to:

- transform actions (like playing a move) to commands to update the game state
- broadcast all these commands to interested parties (game logic and UI)
- apply the commands to reflect the changes

The final result can be seen [here](https://gitlab.com/fpischedda/mazeboard/-/merge_requests/6/diffs?commit_id=ad58453271ce32cf2ef5bbdb0bd329476cef2949)

Changes can be summarized as following:

- Game and UI layers have their own state
- On start, the initial game state is used in both layers
- On user actions a new changeset, in form of commands, is generated
- The commands are applied to the game state
- The commands are sent to the UI layer and applied there too

# Conclusions

We have started with a tightly coupled core and UI logic code base,
slowly we have pushed to the edges the concerns that are more suited 
to each layer; communication between layers is based on `core.async` to
help us to reason about intents (user actions or state changes) and we
have closed the circle with a setup that leverages the abstractions that
we have built to give us the possibility to reason about game and UI independently.

It is not 100% evident at this point, but the UI and game state are still sharing 
way too much structure:

- there is no reason why the game state must be concerned with actions available in a [tile](https://gitlab.com/fpischedda/mazeboard/-/blob/main/src/cljs/mazeboard/ui/components/game.cljs?ref_type=heads#L9)
- there is no reason why the UI state must be concerned by the `:players` key in the game map, given that it does not make any use of it

This will be addressed in the next post, but for now I am happy with what we
have achieved!

# Mazeboard related posts

- [Mazeboard 0](@/blog/clojure-bites-mazeboard-0.md) - Project intro
- [Mazeboard 1](@/blog/clojure-bites-mazeboard-1-dumdom-event-handler.md) - Dumdom event handler
- [Mazeboard 2](@/blog/clojure-bites-mazeboard-2-core-async-to-separate-game-ui-logic.md) - Using core async to decouple UI and game logic
- [Mazeboard 3](@/blog/clojure-bites-mazeboard-3-more-async-to-fully-decouple-layers.md) - Improving UI and game logic decoupling
- [Mazeboard 4](@/blog/clojure-bites-mazeboard-4-commands-to-events.md) - Replacing commands with events
- [Mazeboard 5](@/blog/clojure-bites-mazeboard-5-more-on-actions-cljs-tests-schema-and-future-plans.md) - More on actions, adding tests and future multiplayer architecture
