+++
title = "Closing first sprint"
author = "FPSD"
date = "2023-04-17"
tags = "sprint, summary, updates"
+++

# How it went

In this shot post I want to highlight what happened and first impressions about
the process.

## The process

The idea is to break the development of the website in cycles of one or two weeks.

During this time I've started collecting material about my projects, ideas for posts  
and setting up the development tools and flow.

Each cycle I should produce at least one big or medium sized post about a side project  
and at the same time I should write micro posts to add new material to the site and  
get more visits.

Micro posts worked really well to keep the creative flow and attracting new visits  
to the site. Main topic of the micro posts was Haunt, the static website generator  
and I suspect that talking about tools in this bite sized format works really well.

## How it went

I would say not bad for a new website with no audience other than my personal network  
in the various social platforms, I may also have acquired a couple new followers,  
which is really nice!

Few stats:

- total unique visitors: 36 (39 - my views from different browsers)
- average visitors per day: 6
- unique visitors for the longer sized post: 20

I would say that the site acquired 5 new visitors each day which is good, at least for  
my little experience running a site, not really a scientific analysis but enough for  
this stage.

## Next

This post closes the first sprint started the 2023-04-09.

It took a little longer a week, I was expecting it take a bit longer, and  
probably for deep dives and more content rich posts it will take that much.  
For this reason I am not setting an hard deadline for each sprint, at least  
until I'll find a reasonable benefit by doing so.

I'll start the new sprint (2023-04-17) later today.
