+++
title = "Just release Unrefined's commit 300, an update!"
authors = ["FPSD"]
date = "2023-06-25"
tags = ["clojure", "clojurescript", "unrefined", "product", "update"]
+++

# Just deployed commit 300!

I was working on Unrefined, tested some changes, fixed a bug, cooked a release
and suddenly I've realized that it was commit 300! This requires an update post
for sure ;)


## Bugs

We use [Unrefined](https://unrefined.one) at work and from time to time we experience some weird
behaviors, usually nothing too serious the blocks us but at least one bug
was ruining a refinement session.

We were refining a ticket together with the product manager and we found that
it had to rewritten to make it possible to estimate it. The ticket was broken
down to smaller one and when we came back to the original one, as usual we
started the process to estimate it again and Unrefined start throwing exceptions!
The problem was that a refinement + ticket id are defined as unique and trying
to start a new session for that ticket raise an exception from the DB layer.
We worked around this issue but I was not too happy, so I've created an issue
which I've closed today.


## Restructuring code

The namespaces that make up the application were all over the place and did
not use the pattern "tld.your-domain.app-name" so I've restructured everything
and now all the namespaces are living inside "one.unrefined" namespace; it is
not the most important change in the world but given that I've started working
on the frontend side with the correct namespace model I wanted to be consistent.


## ClojureScript

I've started working on the cheatsheet customization code for the frontend, it
started as a separate project to experiment with it, choosing the framework
(the good old [rum](https://github.com/tonsky/rum) or [helix](https://github.com/lilactown/helix)? went with the familiar one, rum) and the build system
trying out shadow-cljs over the more familiar figwheel-main. After some try and
error I've decided to stick with shadow-cljs because it will be easier to pull
in Javascript dependencies. I still like figwheel-main especially if used with
ClojureScript only solutions (for example [dumdom](https://github.com/cjohansen/dumdom/)).

The custom cheatsheet is still in WIP state and I hope to release it as
soon as possible, it should be a game changer for Unrefined, preparing the road
for user accounts.


## WIP

Some parts of Unrefined are poorly or not tested at all, the core logic is but
I'd like to change this in the near future. It will be almost mandatory as soon
as more users and organizations will use this tool.

Also pending, and depending on user auth, is some sort of SSO or OAuth to leverage
existing platforms like Jira, Asana and friends.


# Closing

My goal with this post is to give an update on Unrefined and what is coming next,
it could have been a tweet but it is too long :D

See you on commit 400! But most probably even earlier ;)

