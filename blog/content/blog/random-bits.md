+++
title = "Random bits"
author = "FPSD"
date = "2023-04-27"
tags = "haunt, bugs, website"
+++

# Topics

This short post will cover:

- guile-commonmark bug
- website next steps
- OBS
- experiment


## The bug that almost got me out of Haunt static site generator

At the time of the second post I was already super excited by the
project, the content was coming out nicely and wanted to have it 
out in the wild ASAP! As usual I launched `haunt build` command to
generate the static pages and all I got was an exception...my mood
went down pretty quickly.

I was sure the problem was on my side, probably something formatted
the wrong way but I could not make it to work. I was thinking it was
a bug in Haunt itself but after carefully inspecting the stack trace
I've tracked down the problem to [guile-commonmark](https://github.com/OrangeShark/guile-commonmark),
for which I've opened an [issue](https://github.com/OrangeShark/guile-commonmark/issues/22),
hoping to get some feedback soon(ish). Unfortunately I've quickly realized
that the project is not actively developed, and decided to find a 
workaround for it. Even worse, the test suite is failing so it is not
easy to understand if I am breaking other places.

At some point I've identified the line that was breaking and commented
it, only because it was doing some cleanup, and having some extra 
spaces or unneeded HTML tags in the output is not the end of the world.

If curious or you are affected by the same problem, this is the line to
comment [link](https://github.com/OrangeShark/guile-commonmark/blob/master/commonmark/blocks.scm#L272).

p.s. I think this happens because I am compiling with Guile 3.0.x, maybe with 
Guile 2.x it works well...but I haven't tested it yet.


## Website next steps

I want to keep the website features to the bare minimum but at the same
time there are some missing bits:

- Links to contacts and social accounts
- Project pages
- Tags
- Maybe a page for the CV?

I will slowly work on these parts, there is no rush but at least it is written
somewhere publicly and not only in my "secret" project files :) .


## OBS

I've started learning to use OBS because I want to record a couple of videos to
explain how [Unrefined](https://unrefined.one) works and how the Chrome Extension
may improve the workflow.

I've got a course on [Udemy](https://www.udemy.com/course/start-streaming-with-obs-studio-in-2022/)
and I am liking it a lot so far! (Not affiliated).

Frankly speaking, one day I would like to try to stream a coding session on one of
my projects to get live feedback and have some fun! I am an introvert and I am 
postponing this step every week, but at some point it will happen, so stay tuned!


## The experiment

This time I'll limit the number of places where I'll advertise my posts in order to
understand how much recurring traffic I have, if any. Clearly marketing helps a lot
but at times I feel I am a bit spammy and I am considering to promote fewer posts
in future. Lets see how it goes :) .
