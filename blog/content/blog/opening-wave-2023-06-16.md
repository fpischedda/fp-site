+++
title = "Opening wave 2022-06-16"
author = "FPSD"
date = "2023-06-16"
tags = "wave, plan, product, unrefined"
+++


# Starting Wave 2023-06-16

After a two days break I am ready to open the new wave, that will last
one month like the previous one, I feel comfortable with this pace!

What's in the plan? It be broken down to the following items.

Before diving into the wave's goals, the usual link to the [MR](https://gitlab.com/fpischedda/fp-site/-/merge_requests/10) 
that will collect all the work being done and shared later in this site.


## Unrefined

Given the seminal work of the previous wave the plan is to finish the
customization of cheat sheets. The initial implementation of the UI is
quite rough but I can focus on improving it later if there is enough
interest, now it is time to embed it into the app. The extension may
come next but realistically in the next wave.

Another key addition to [Unrefined](<https://unrefined.one>), pending for
too long, is the landing page and some content explaining what problems
it is trying to solve and how, namely estimation bias and why the estimation
cheat sheet is a core function of the flow.

Authentication has been mentioned before and delayed for too long, it is
now time to add it, probably using an external identity management platform
like [Clerk](<https://clerk.com>).


## fpsd.codes

The plan is to continue publishing at least two Clojure Bite articles and
updates regarding product development; posts about Unrefined will be
replicated in the upcoming application blog.

If possible I'd like to also include updates on the second organization
the should be onboarded to Unrefined, if it will even happen ;).

Redesigning the general layout, like grouping posts be month, adding
contact pages etc will be postponed to the next wave.


## Live streaming

Still unsure how to drive more value while streaming, I guess I'll
keep trying to learn how to make something out of it.


## Closing

This post sets the start of the new wave, the feeling is that the plan
is more clear compared to the previous one, I hope that the closing post
will confirm that it was a good plan, or at least that I've learned how
to improve for the next time, see you around the net!

# Discuss

- [Twitter](https://twitter.com/focaskater/status/1669763899858583557?s=20)
- [Linkedin](https://www.linkedin.com/posts/francesco-pischedda_starting-wave-2023-06-16-activity-7075543297842192385-f7CN?utm_source=share&utm_medium=member_desktop)
- [IndieHackers](https://www.indiehackers.com/post/my-build-in-public-process-and-operating-system-hows-yours-be50155f11)
