+++
title = "New sprint started!"
author = "FPSD"
date = "2023-04-18"
tags = "sprint, post-merge-request"
+++

# [2023-04-18] A new cycle of post is starting

I enjoyed my first week with this new blog!

The pace is almost right, I don't feel overwhelmed by my expectations,  
and I think I can find the time to write a bit more.

Priority for now is to get back to Unerfined and finish some pending  
work regarding:

- Analytics: add Plausible, yes I have other logs but are not nearly as useful
- Improve the cheatsheet, it is too specific to my employer needs
- Finish and release the browser extension, potentially targeting more browsers

On the micro posts side I have not much at hand yet:

- Few words on a bug in the guile-commonmark library
- Updates on the site analytics and making them public
- Adjustments to the site's layout, linking to my social accounts

As expected, the source material will be written
in `docs` directory and tracked in a fresh new branch and MR so that I can receive comments
and suggestions, the MR is available [here](https://gitlab.com/fpischedda/fp-site/-/merge_requests/6).

Feel free to stop by, even to just say hello :) .
