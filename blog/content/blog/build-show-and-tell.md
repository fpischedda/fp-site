+++
title = "Build, show & tell"
author = "FPSD"
date = "2023-05-18"
tags = "buildinpublic, buildshowandtell, community"
+++

# TL;DR Connect with your community! Right, now!!!

Yesterday, May 16 2023, 19:00 CEST, I've attended a spontaneous
meetup of indiehackers, organized on Twitter by [GYN](https://twitter.com/gyulanemeth85).

Somehow he managed to take a group of strangers, posting under the
hashtag [#buildinpublic](https://twitter.com/hashtag/buildinpublic?src=hashtag_click), and gather everyone together to talk about
what we are building, what kind of help we are after and how we can
help others. And was great!

Here is where it all [started](https://twitter.com/gyulanemeth85/status/1658885816498651180?s=20).


## The meetup

We started introducing ourselves, our background and our projects,
after that [Alexander](https://twitter.com/axmacinsky) introduced his projects and I've assisted to
the most honest and insightful round of feedback that anyone can
receive in her/his life by complete strangers!

Honestly it has been amazing, ideas floated around, from UX to
monetizing, to cost analysis and more. And just to repeat, it was
coming from people that have never interacted in real or virtual life!


## Closing words

I wrote this tiny post to encourage everyone to get in touch with their
own community, you'll never know how supportive it can be until you try
it. It is easy to set the boundaries to your (home) office and users'
feedback, but if don't get in touch with other experiencing the same
challenges as yours, I can confidently say that you are missing out.

Please come and join the [#buildinpublic](https://twitter.com/buildinpublic) community, if don't like it you
lose nothing but I suspect that we have a lot to share and learn from
each other.
