+++
title = "Plans to improve the estimation cheat sheet"
author = "FPSD"
date = "2023-05-20"
tags = "unrefined, clojure, product, cheatsheet"
+++

# Project

Project name: Unrefined

Project URL: <https://unrefined.one>

Project sources: <https://gitlab.com/fpischedda/unrefined>

# Cheat sheet redesign

The current cheat sheet is not ideal for the following reasons:

- It is hardcoded and there is no way to chose a different one
- It is too specific to one use case/company
- The estimation UI with the sliders is not intuitive, hiding the maximum story points


## Breakdown of tasks


### Change UI to select a cheat sheet from a list of available ones

When starting an estimation provide the option to select the cheat sheet to be
used during the refinement session from a list of available ones, possibly with
a preview of the breakdown items.

#### Sub tasks - Size M

-  [BE] Provide the list of available cheat sheets to the frontend layer with their details
-  [FE] Show the pre-selected cheat sheet name
-  [FE] Send the selected cheat sheet to the backend when starting a refinement
-  [BE] Use the cheat sheet sent by the frontend when creating a new refinement
-  [FE] Enable selecting a different cheat sheet from a list of available ones
-  [FE] Show the breakdown items of the currently selected cheat sheet


### Change UI of the estimation page

Explore few different designs for the estimation page to enable new ways of
selecting the story points for each breakdown item. Implement at least one
and provide a way to select between available styles, starting from the
current one. If a user changes the style, set it as default in the local
storage.

#### Sub Tasks - Size S

- Try a couple of designs before implementing them
- [FE] Implement at least one new design
- [FE] Enable selecting from available designs
- [FE] Set the selected design as the default in the local storage
- [FE] Read the selected design from local storage and use that if available otherwise use the default one


### Customize the cheat sheet

After selecting a cheat sheet, the user can customize it, creating a new
cheat sheet.

It should be possible to start from a blank page if the current templates
do not match the user's use case.

Evaluate if it is feasible to store the new cheat sheet somewhere, considering
that at the moment there is no way to authenticate a user.

#### Sub Tasks - Size L

- [FE] Enable cheat sheet customization, adding or removing breakdowns and their items
- [FE] Send the custom cheat sheet to the backend when starting a new refinement
- [BE] Accept new cheat sheets from the frontend and use them when creating a refinement
- [BE] Send the custom cheat sheet to the frontend in estimation and result pages
- [FE] Use custom cheat sheets in the estimation and result pages
- Evaluate how to store and retrieve custom cheat sheets

