+++
title = "Checkpoint 2023-11-01"
author = "FPSD"
date = 2023-11-01
tags = "retro, future work"
+++

# [2023-11-01] What's up?

Hello everyone, long time no see!

After a break I've finally found new topics to work on and write about but,
before jumping to the upcoming plans lets take a look of what has happened
meanwhile.

# Unrefined

[Unrefined](https://unrefined.one) is the closest thing to a product that I have developed in the 
past year, it is currently used at $WORK and I have tried to understand if
it could be valuable to other teams. The feedback I've received was not great, the
most positive comments were "interesting" and "I see the value proposition" but 
it haven't move past that and, worse, haven't reached the point to start a trial with
the teams I've been in contact with. At least I've had a lot of fun working on it and
learned a lot in the process, so the glass is still half full ;)

Unfortunately I've realized that to move it forward it would require a non trivial
effort compared to the potential revenue I can expect from it, based on the feedback
I have received by potential users. It will be on maintenance mode for my only user,
and if someone else wants to try it, it is still free (as in beer and in speech).

# Clojure Bites

The series covered simple topics, mostly targeting Clojure beginners like
myself and usually centered on the work I was doing on Unrefined. After few posts,
I've had an hard time finding interesting topics to cover, which is reasonable given
that I was not progressing with Unrefined and writing about something not directly
useful was feeling a bit too artificial. If I have not experienced a problem that I
wanted to solve I find it hard to give an honest perspective on the solution that I 
am covering.

This does not mean I haven't tried, this is a list of topics I had (have) in the
backlog, loosely based on my own requirements for Unrefined, but not limited to it:

- Using Keycloak as an identity provider for Unrefined.
- Enabling custom estimation cheat sheets in Unrefined...which required a lot of frontend work I was not ready for.
- Building on the previous point I spent some time evaluating what options are available was in the frontend space, figwheel-main VS shadow-cljs, reagent VS rum VS helix/UIx2.
- Creating a service to simplify Server Sent Events solutions that scale; based on what I've seen in Unrefined, it might be useful to rely on a third party solution to handle SSE traffic.
- Using Clerk notebooks to work on support tickets at work as a way to document the process and create a common code base.
- Using Datascript as the state of an Entity Component System library for ClojureScript; this looks extremely exciting and I had a couple of games I can adapt to this approach.
- Using what I've learned from Unrefined to, finally, move one simple game (Mazeboard, ore on this later) to the next step, enabling realtime multiplayer games and experiment with its logic.

So yeah, so many topics at hand but I did not focus on any of them, and I was stuck (and it sucks!).

# Next steps

Taking Unrefined out of the equation is making it easier to decide where to spend
my time, optimizing for having fun while learning something, and the winner is:
revisiting Mazeboard's code base to finally have a way to test its gameplay, with real players, building on top of what I've learned in the past months.

This will cover so many topics, possibly creating material for posts targeting people
with different levels of experience in Clojure and its tools/libraries, for example:

- Setting up a [Shadow CLJS](https://github.com/thheller/shadow-cljs) project
- Introducing a frontend library (or two just to show different approaches, I am looking at [dumdom](https://github.com/cjohansen/dumdom/) now)
- Working on UI components and previewing them with [portfolio](https://github.com/cjohansen/portfolio)
- Live multiplayer game updates using SSE
- Persisting the game state (probably with some sort of Datalog DB)

I think these are reasonaly simple topics to get started again, fueled by the future
work on Mazeboard. After this step, it will be possible to focus on broader topics,
like identity management systems, scaling, logging and monitoring and who knows what.
At least at this point the basics will be covered and experience should suggest
what/where to look at.

The plan look very interesting (at least for me) and doable, and knowing that I am
back again at experimentation VS building a product (that no one needs, anyway) is
quite exciting!
This is my sweet spot, happy to be back at it!

# Mazeboard

The game should be a rogue like board game where player explore a maze (I haven't
decided on theme yet, a dungeon? An alien spaceship?), trying to get a treasure and
bring it back at the starting position, other players can move towards the goal or
prevent other to win; it should be possible to print the board pieces and play with
a normal dice or a coin (I still have the paper prototype somewhere...). It is not a
surprise that it looks boring, I've had the idea during a boring meeting after all! :)
Maybe it will turn out as a playable but I will never know until someone will play
with it; relying on local, in person tests, is for sure out the
equation, so online game it is!

Few years ago my go-to approach was to build the logic on a sorts of backend, just to
abandon the project when it came the time for the user facing part. This is silly!
Now I've leaned that having a user facing prototype is much, much more valuable, so
I am revisiting Mazeboard's code base to quickly provide a way to play it, favoring
user facing work.

Sources will be available [here](https://gitlab.com/fpischedda/mazeboard), I am halfway
on re-doing the frontend part, at least the UI components, and I have started using
dumdom + portfolio; excluding few headaches when setting everything up, so far the
experience have been quite positive!

# Conclusion

Finally I am back again at the whiteboard, experimenting with ideas and libraries,
gaining experience and hopefully giving back in form of posts or live coding sessions.
(quite unlikely but I'd like to try again! ;) )
