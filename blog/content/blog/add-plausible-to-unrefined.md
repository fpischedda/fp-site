+++
title = "Who is using Unrefined?"
author = "FPSD"
date = 2023-04-18
tags = "unrefined, updates, analytics"
+++

# Who is using Unrefined?

Excluding my colleagues at my day job I don't know!

Recently I have shown [Unrefined](https://unrefined.one) to the world but I have no idea if  
someone else is using it. Yes I have "some" logs but those are not really useful  
to track visits, more for debugging purposes.

Given that I am liking [Plausible](https://plausible.io) so much I have  
added their analytics script to [Unrefined](https://unrefined.one).

As always, it is as easy as adding a script tag, [here](https://gitlab.com/fpischedda/unrefined/-/merge_requests/42) is the change.

That's it for today, as always feel free to share feedback or just say hello :)
