+++
title = "Closing wave 2022-04-26"
author = "FPSD"
date = "2023-05-15"
tags = "wave, progress, action-items"
+++

# Overview

Closing the first wave, I feel the need of a sort of retrospective, pointing
out what went well, what didn't and action items for the next iteration.

TL;DR is that I've probably set ambitious goals for myself (maybe too much),
there are two driving forces competing for my limited time, the content creation
side and the product development side, and I have to find a way to balance
my time budget.

Continue reading for the gory details.


## Wave goals

From the start of the wave

- Improving the estimation cheat sheet in Unrefined
- Creating a landing page and marketing it (throwing some $$$ in marketing maybe)
- Improving this website, there are still some information missing
- Reveal the new experiment and first impressions


## What went well

### Improving the estimation cheat sheet in Unrefined

This is a mixed bag, on one hand I've received some feedback that the cheat sheet
is a good idea, but in the current form is not that useful. I've started working
on a generic one but it is not an easy task but now it is clear that I can try
to create some default templates and let people customize their own.


### Creating a landing page and marketing it (throwing some $$$ in marketing maybe)

Baby steps, I have added a brief description of how Unrefined works; next step
should be to get a pricing page proposing "Pro" features and measure engagement.


### More content and hints about what kind of posts drive more visits

In this wave I've published two ([1](/clojure-bites---clojuretestare.html), [2](/clojure-bites---dynamically-add-depencencies-at-runtime.html))
short articles on Clojure which performed quite well given my small network.
One lesson is that this kind of posts do not perform well on Linkedin, on the other
hand Reddit and Twitter convert pretty well. Also timing is crucial, better to post
early or mid week.


## What didn't


### Improving this website, there are still some information missing

Nothing done yet, plus I wanted to record a screen cast but I've spent too much time
learning OBS and I was not happy with the final result. I have the video still
in my todo list and I want to release it soon!


### Reveal the new experiment and first impressions

No work done here as well. I'm still figuring out the scope of the project which
at this time looks way too big for a single person. I've got other ideas that
I'd like to validate. The biggest blockers are:

- Defining scope and core idea
- Learning how to quickly build landing pages, it is not a tech problem, more of copy writing


### Marketing Unrefined

It was planned to record a video session of how it works, create a landing page
and push it to the wild. None of this happened.


### Time management

Currently I am spending most of my time on content creating and not enough on current
projects, adding meaningful features or potential projects that require some research.

On content creation, I am still finding my way to it and it is time consuming but I suspect
there is a lot of potential there, to drive traffic to products or possibly becoming
a product itself. The general suggestion is to try to find verticals, and this is a whole
big topic to explore.


## Action items

Looking at the broader picture I think I have to:

- Get better at managing time: with a full time job every "free" time matters!
- Build an operating system that makes sense: connected to previous point; having a process should help with time management as well
- Keep ambitious goals, even if I haven't reached most of my goals I think it is better to aim higher than lower
- Try out more ideas i.e. get better at pushing out landing pages and looking for early feedback/validation


## Closing words

It is important to remind to my(your)self that building something new is hard but
also exciting! There are a lot of unknowns and we learn as we go, so please try to find
the time to look back at what you did, where you want to be and look forward the next
steps.

