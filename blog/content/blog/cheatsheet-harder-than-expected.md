+++
title = "Unrefined generic cheatsheet"
author = "FPSD"
date = "2023-05-06"
tags = "unrefined, product, cheatsheet"
+++

# Project

Project name: Unrefined

Project URL: <https://unrefined.one>

Project sources: <https://gitlab.com/fpischedda/unrefined>

## Overview

Unrefined's UX for estimations is centered on the estimation cheatsheet,
with the assumption that it would help engineers to breakdown tasks to
smaller items and assign a number to each of them.

A cheatsheet provides a list of breakdown items such as:

- Implementation
- Testing
- Migrations
- Risk
- ...and more

For each item there is a description of the possible actions for example,
considering implementation:

- Small change to an existing functionality: 1 story point
- New endpoint for an existing domain model: 3 story points
- Implement two or more domain models with their endpoints: 8 story points

With the guidance of the cheatsheet, over time it will become easier and 
more predictable how to estimate a task, getting to consensus much faster.

One thing it does NOT is to make estimations more ACCURATE! Estimations are
gut feeling predictions of how much it could take to complete a task, and
should NOT be used to set deadlines.

Here is how it looks in Unrefined's estimation section

![cheatsheet][cheatsheet-image]

[cheatsheet-image]: images/unrefined-cheatsheet.png "Default cheatsheet"


## The problem

The current implementation comes from our experience at $work so it is
very specific to our needs, making not so useful for other team in a 
different domain, with different tech stacks and so on.

For this reason I have started to abstract away the breakdown items and 
their description but it is clearly very hard to come up with something
that can capture every possible permutation of stacks, habits, business
domain and what not. This is a source of frustration because it makes
it harder to market the tool to other teams.


## Going forward

I am not sure how to proceed with the cheatsheet but I have some ideas:

- Create a generic one, knowing that it will not be a perfect fit for all cases
- Start collecting feedback and create specialized cheatsheets
- Give people the opportunity to create their own cheatsheet

I think that all of the above options are viable and would help to make this
tool more useful to a broader audience, maybe even outside the software
engineering circle.
