+++
title = "Starting work on next post"
author = "FPSD"
date = 2023-04-09
tags = "pre-post, post-merge-request"
+++

# [2023-04-09] Started collecting material for a new blog post

According to my plans I am supposed to create a blog post announcing that I am starting
to work on a new post, so here we are.

As expected, the source material will be written
in `docs` directory and tracked in a fresh new branch and MR so that I can receive comments
and suggestions, the MR is available [here](https://gitlab.com/fpischedda/fp-site/-/merge_requests/1).

Feel free to stop by, even to just say hello :) .

The next post should be a presentation of the projects I am working on but I am not sure 
if I will focus on a single project or if the post will be an overview of what is 
currently in the pipeline.

Another aspect that I want to tune is how much I should say in the sprint start post,
for today I am keeping it simple.
