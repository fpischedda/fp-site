+++
title = "Migrating to Zola SSG"
authors = ["FPSD"]
date = "2023-07-25"
tags = ["buildinpublic", "SSG", "zola"]
+++

# Overview

Recently I switched from [Haunt](https://dthompson.us/projects/haunt.html) to [Zola](https://www.getzola.org/) as the static site generator for
this blog. Main reason was a bit of frustration with Haunt's documentation
and the CommonMark implementation it was using which was holding me back.


# Why the switch

When starting this blog I wanted something simple and potentially hackable,
in terms of being able to play with the generator like any other Lisp program,
modifying it a runtime and seeing the result of the change right after it happened.

While Haunt is totally capable of doing so the first roadblock (at least for me)
is that it is written in [Guile](https://www.gnu.org/software/guile/) which I don't know at all; given that it is a
Lisp I hoped to get into it quickly, but I've never studied it or played with it
too much, and there is only so much time in a day so I cannot play with all the
fun things that there are around and at the same time hope to get some work done!

So I started looking for alternatives, maybe leaving aside the hackability of the
underlying system in favor of focusing on writing content.

Don't get me wrong, Haunt is very nice and customizable, it is not just a toy and
for example it is used to build the [Guix](https://guix.gnu.org/) website (and others); it is possible to
look at that site sources to get an idea on how to write a great website!
The problem is that I did'n manage to learn few fundamental things, Guile and
Haunt internals, quickly enough to be confident with this system and be productive
with it. Better luck next time, maybe.


# Candidates


## [Cryogen](http://cryogenweb.org/)

Language: Clojure
Documentation: While complete the feeling is that it does not flow too well

Pros:

-   You have the engine at your hand and you can hack on it easily while running
-   Easy to start

Cons:

-   Custom front matter format (EDN)
-   Takes some time to get into it, not ideal if you just want a quick blog with your style
-   Documentation could be better


## [Nikola](https://getnikola.com/)/[Pelican](https://getpelican.com/)

Language: Python
Documentation: Complete

Pros:

-   Battle tested
-   Extensible (with lots of extensions)
-   Lots of themes

Cons:

-   Custom front matter
-   Too big for my needs


## [Hugo](https://gohugo.io/)

Language: Go
Documentation: Complete

Pros:

-   Battle tested
-   Extensible (with lots of extensions)
-   Lots of themes (I mean LOTS!)
-   Comes packaged in a single binary

Cons:

-   I could not find a clear path to get to my first post quickly
-   Having a lot of features I felt lost in the docs, which caused friction/churn
-   Too big for my needs, the home page says "The world’s fastest framework for building websites"


## [Zola](https://www.getzola.org/)

Language: Rust
Documentation: Complete

Pros:

-   Documentation was feeling just right for my needs
-   The home pages says "Your one-stop static site engine", just what I need
-   Quick time to first post and custom layout
-   Lots of themes (and more are coming to it)
-   Comes packaged in a single binary

Cons:

-   Maybe not as feature rich as others (not a problem for me)


# Final decision

Admittedly I did not run a very scientific comparison process, what I did was
to try to run a generator with its defaults, hack it a bit and measure:

-   time to get to the same layout and a first post
-   level of frustration while doing so
-   documentation
-   &#x2026;gut feeling

What I did not care about:

-   speed of generation of content
-   programming language
-   availability of themes

Following the principle of "least resistance path" I went with Zola because
the process to get to that first post was really painless! Some engines were
not properly documented, with other I felt overwhelmed by options or in the
end it did not feel just right.

It took me 10 minutes to get Zola's approach and file structure, another 30
minutes of grunt work to port the front matter of posts from Haunt (I could
have automated it but it was too hot that day and I was not able to think too
much, but it would have been a nice exercise with Babashka!).

While others may be more complete solution to build complex websites, what I
really want is a tiny container for my posts, potentially some other pages
like contact section, bio etc. and Zola was able to deliver in just no time.

Your requirements may be totally different and other engines may work better
for you, all I can suggest is to try (some of) them and see which one sticks
to you!

