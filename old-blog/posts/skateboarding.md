title: Skateboarding, keep track of your progress
author: FPSD
date: 2023-06-29 21:08
tags: boostrapping, experiment, skateboarding, accountability
---


# Overview

Keep track of your skateboarding progress, set your goals
and create a plan to achieve them at your own pace.


# Possible names

-   skoach
-   coach360
-   pops
-   scoop


# Application platform

-   website, mostly for offline activity like loading media, set plans
-   mobile app, for real time updates


# Features

-   record where you are, tricks you own trick you want to get
-   set monthly/weekly/daily plans and get stats
-   gamification, achievements, challenges
-   suggest plans and workout activities


# Copy for landing


## Random ideas

From skateboarders to skateboarders.

Land that trick that is always escaping you, keep track of your
progress, learn from your mistakes.

Skateboarding is freedom, and that great feeling when you land a
trick.

Keep yourself accountable, share your progress with your crew, set
personal and group challenges.

If it is not recorded it didn't happen, record yourself and share
the videos on social media platforms.

Achieve your goals and unlock new challenges.

Remember to have fun!


# Business model

-   Freemium
-   Pro with subscription or LTD


# Longer term vision

-   For amateurs going pro
-   For teams, either brand or national
-   Live coaching, talk to a pro, show videos and get help
-   Workout coach
-   Tools for content creation
-   Public challenges with sponsors

