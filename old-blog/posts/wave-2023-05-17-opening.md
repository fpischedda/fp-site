title: Opening wave 2023-05-17
author: FPSD
date: 2023-05-17 22:00
tags: wave, progress, retro
---

Wave merge request: <https://gitlab.com/fpischedda/fp-site/-/merge_requests/9>

# Overview

Past wave had a bit ups and downs, following the white rabbit of
number of views on this website but not moving the needle of [product](https://unrefined.one)
awareness, nor investigating other opportunities. A key factor was
(poor) time management, as reported while closing wave 2023-04-26.


## Next round

This wave should focus on the failures of the previous one, trying
to address the issues that have had the most impact on the expected
progress, such as:

-   Balance: main driving forces are content creation and product development, how to respect both?
-   Progress: how to make sure energy is spent in meaningful activities
-   Time management: how to make use of the limited time available


## Projects

As always there is work to do for this website, Unrefined and, this
time I am adding a new idea that I'd like to validate.


### Website

Topics:

-   Clojure Bites: I am enjoying writing this series of posts, realistically I want to have one every two weeks
-   Project progress: not much happened recently, I want to try to have an update once a week
-   Project validation: this is a new thing, I have a couple of ideas in mind but I have to scope them well before starting
-   Video contents: I'd like to record a session about the last Clojure Bites and how to use Unrefined; it is a time consuming activity


### Unrefined

I'd like to use some of the feedback I've got recently to move
the project to a state where it is more generally useful and usable:

-   Estimation cheat sheet: it is an interesting concept that must be generalized
-   Cheat sheet customization: it is hard to make a generic one and I should enable people to customize it
-   Estimation breakdown UI: the current slider approach can be improved, I've collected a couple of ideas worth trying
-   User account + OAuth providers: if users can customize their cheat sheets they may want to keep it with their own accounts


### Validating a new idea

A general advice is to build something that solve your problems
with the assumption that others may have the same problem. But
before building the solution is better to validate that there is
enough market for it both in terms of potential users and $$$.
This is the indie hacker/bootstrapper 101, and given I'm a noob
better to start from the basics!

So the plan is:

-   Find where the potential audience hang out
-   Understand their needs
-   Create a landing page and collect potential users

I'll reveal the project and results in a future post, until then
stay tuned!

### Stats update

Here is the graph since the beginning of this site, it awesome how
it growed in just one month! Peaks are for the Clojure Bites series
but just because it is easier to target specific communities, and
not because those were great articles :) 

![stats](images/all-time-metrics-2023-05-17_22-09.png "stats")

### Discuss

[Linkedin](https://www.linkedin.com/posts/francesco-pischedda_opening-wave-2023-05-17-activity-7064699255726182401-TYjQ?utm_source=share&utm_medium=member_desktop)

[Twitter](https://twitter.com/focaskater/status/1658934922843701250?s=20)
