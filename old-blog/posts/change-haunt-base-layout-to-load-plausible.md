title: Customizing Haunt's base layout
author: FPSD
date: 2023-04-12 22:00
tags: haunt, learing, guile
---

# [2023-04-12] Learning how Haunt's theme/layout system works

## I just want to embed a script!

[Yesterday](/adding-analytics.html) I've integrated [plausible.io](https://plausible.io)
into the website. The process is extremely simple, like in most cases it is as easy
as adding a script tag to embed the tracking code and you are done.

Most static site generators usually have a set of template HTML files that are
filled with posts index, post contents and other stuff, but Haunt is a bit different,
as it provides its own template language that in turn will generate the HTML code.
The DSL reminds me a bit [Hiccup](https://github.com/weavejester/hiccup), just replace
vectors with lists and keywords with symbols and the two are basically the same...if we 
don't focus too much on the details.

This is the first time I am using Haunt and I had to do a bit of digging to figure out
how to add the tracking script. Fortunately there are plenty of examples, starting
from the project's documentation [sources](https://git.dthompson.us/haunt.git/tree/website),
where it is possible to see how the author is [embedding](https://git.dthompson.us/haunt.git/tree/website/haunt.scm#n79)
piwik analytics in the project's website.

Other site built with Haunt (together with sources) are listed [here](https://awesome.haunt.page/).

## Site configuration file

Haunt expects a configuration file called haunt.scm, with the settings of the site like
domain name, site title, where to find the source posts etc in order to be able to build the
final site. Part of the settings are which readers to use, for example [CommonMark](https://commonmark.org/),
or builders such as blog, feeds, static assets etc.

Each builder is customizable and the `blog` builder accept a `#:theme` parameter where it is
possible to provide a custom theme; when not specified the builtin
[ugly-theme](https://git.dthompson.us/haunt.git/tree/haunt/builder/blog.scm#n126) is used.

The theme is declared using the `theme` function that accepts the following parameters:

- name: name of the theme.
- layout: a function that generates the HTML layout of the site.
- post-template: a function that generates the HTML template of a single post.
- collection-template: a function that returns the HTML template to represent a list of posts.
- pagination-template: a function that returns the HTML template used for the pagination of posts.

Fortunately each parameter is optional so, to just customize the general layout to
include the script for the tracking code, it is enough to create a new function,
overriding the base layout function, to include the custom code and that's it! Ez Pz

Here is my current custom theme in all its glory

```
(define (fpsd-default-layout site title body)
  `((doctype "html")
    (head
     (meta (@ (charset "utf-8")))
     (script (@ (type "text/javascript")
		(defer "true")
		(data-domain "fpsd.codes")
		(src "https://plausible.io/js/script.js")))
     (title ,(string-append title " — " (site-title site))))
    (body
     (h1 ,(site-title site))
     ,body)))

(define fpsd-theme
  (theme #:name "FPSD"
         #:layout fpsd-default-layout)) ;; <- please notice that I have defined only the layout
```

# Conclusion

It is entertaining to play with new tools, sometimes we expect them to work in a certain
way and it can be frustrating if they don't but, if we spend enough time trying to
get to understand them, then we can learn something new and even have some fun time!
