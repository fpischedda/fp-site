title: Introducing Unrefined
author: FPSD
date: 2023-04-16 13:30
tags: unrefined, clojure, product
---

# Overview

Project name: Unrefined

Project URL: <https://unrefined.one>

Project sources: <https://gitlab.com/fpischedda/unrefined>


## What is this about

Unrefined aims to help remote software engineering teams to run ticket refinement  
and estimation sessions.

Refinement sessions are usually attended by:

-   PMs: presenting their tickets and looking for estimations to plan next sprints
-   Engineers: refining tickets and providing estimations for them
-   EMs: coordinating the whole session

Unrefined mainly targets EMs who coordinate the refinement and estimation sessions,  
and Engineers who are in charge to break down the ticket in its actionable parts  
and give an estimation of the effort needed to implement them.


## Why

I've started Unrefined essemtially to scratch my own itch. At work we tried different  
approaches and all of them never worked properly. Only one was really cool, a Zoom  
bot developed by one of my colleagues, but at some point we left Zoom and I missed it  
a lot. After that we used to cast our estimations in the chat of the video conferencing  
tool we switched to. And that had few problems:

-   Estimation bias: sometimes people wait for other votes to express their own
-   A bit hard to coordinate and to find out the final estimation

What I liked about the bot:

-   Everyone's estimation was secret so there was no bias
-   The bot spitted out the final estimation or pointed out if more discussion was needed
-   Easy to use


## Development process

### Initial work

Based on the features provided by the bot, I have started thinking about how to structure a  
web app or an api to manage these sessions, the number of features started growing  
without even thinking at the core logic: provide a way to express estimations and give  
out a result (either an estimation or a suggestion to discuss more).

At this point I've decided to scratch all of the unneded features and started to implement  
the core logic, without even thinking about how people will interact with it.

I have opted for [Clojure](https://clojure.org/) because I like its interactive,  
iterative development flow, being able to shape the program while it is running,  
getting a taste of how the API would be used while using it.  
There are many more reason to choose Clojure for your projects but this is not the right  
place to talk about that.

During a lazy Sunday I have quickly sketched out the core, which can be seen at this
[commit](https://gitlab.com/fpischedda/unrefined/-/commit/ea410a5c0c9bddd5c805e5ad054a44fcf2b8f947).  
(Yeah, the commits are almost always a mess&#x2026;but I'll get disciplined later)

At that point I was so excited by its simplicity that I wanted to provide a way  
to actually use it!

### Making it usable

Having built the foundations it was time to think about how people would use it.

Goals:

-   Easy to access and use, people should be able to create and join sessions with the least effort possible
-   Easily get to the ticket details by linking to the team/company ticketing system
-   Avoid estimation bias by hiding all estimations until the final result is shown
-   Instantly get the estimation result
-   Easily estimate a new ticket if any

This is the flow that I came up with:

-   EM puts the ticket URL in the home page form field
-   EM gets the estimation link for engineers and shares it
-   Engineers discuss the ticket and express their estimation in the estimation page
-   EM + Engineers get realtime updates of the estimations currently expressed (now there is no bias)
-   EM gets the final estimation, updates the ticket with the resulting story points
-   EM starts a new estimation for the next ticket if any
-   Engineers browsers will update automatically to the next estimation page

This flow stayed the same since the beginning and it worked quite well, at least for our  
needs at work.

One feature that came after the first draft is the estimation cheatsheet.

### Estimation cheatsheet

Our estimation cheatsheet is a guideline that helps everyone to stay in the same page  
when expressing the estimation and improve coherence, but this does not mean that  
estimations are always equal because someone may include effort not considered by othes  
like test, migrations and so on.

How the estimation looks like? I proposes a breakdown of activities, for example:

-   implementation: code changes from small ones like adding a field to a response JSON or bigger ones like adding a new endpoint
-   automated tests: how many test should be added? If there is some tech debt test may require more effort
-   database migrations: are we just adding/removing one field? Is a data migration needed?

&#x2026;and some more entries, but you get the point.

Adding the cheatsheet to the estimation page helped a lot to have more predictable and  
coherent estimations. This is helpful for the product team too because, by looking at  
past tickets they my have a better idea of the effort required by a new feature.

### Moving forward

Unrefined is now actively used at my workplace and I am proud of the outcome and I think  
it could be implemented in other organizations. But before that I want to generalize the  
estimation cheatsheet and provide a way to create a custom one.

Another feature I have in progress is the browser extention to help EMs to run estimation  
session. The extension is almost there but it needs some more testing.

### Next steps <code>[0/3]</code>     :NEXT:

1.  TODO improve the cheatsheet management

2.  TODO finish the browser extension, test it and port it to more browsers

3.  TODO try to get feedback from other organizations or engineering teams

### Feedback please

In case you want to share some feedback please come over to the project [repo](https://gitlab.com/fpischedda/unrefined), it is
open for everyone! 

