title: Closing wave 2022-05-17
author: FPSD
date: 2023-06-12 21:15
tags: wave, progress, buildinpublic, product, unrefined, metrics
---


# Overview

Every wave is a great source of learning material, built on top of the
previous experience and the expectations I set for myself. So what
have I learned this time?

To summarize:

-   reaching out to the builders/solopreneurs community is a force multiplier
-   time management is still an issue but on its way to be addressed
-   same for time allocation, should I spend more time writing or building?

In this post I'll try to put down an honest analysis of the past wave, and
set the ground for the next one.


## Some metrics


### fpsd.codes

First of all the raw numbers, the blog have reached the all time high of 1K
unique visitors! It does not look like a lot, and it isn't, but I have to
consider that the main traffic comes from Clojure Bites posts, and it is a very
small niche. I enjoy writing those posts, they seem useful in general and not
just related to my effort to create and advertise my products&#x2026;but they help in
that sense too.

Main traffic sources are:

-   Reddit
-   Twitter
-   Linkedin
-   Dev.to
-   Clojure Deref

There are other sources but those are both irrelevant and surprisingly I
don't market my posts there! Weird but interesting.

I can confirm that some channels perform better than others depending on
the content of the posts; more technical posts perform great in Reddit
because it is easy to directly reach the interested community. Twitter
performs well too, probably because some aggregators like [PlanetClojure](https://twitter.com/planetclojure)
will re-post to interested communities. A new entry in this wave is
[Clojure Deref](https://clojure.org/news/2023/06/09/deref), a weekly update about all things Clojure; it collects,
videos, podcasts, blogs and libraries updates, highly suggested!
To try in future, again for Clojure related posts, is the channel
\#news-and-articles in [Clojurians Slack](https://clojurians.slack.com/messages).

One trick that I have learned is to publish a Clojure related post on Wednesday
or Thursday in order to be featured in the next edition of Clojure Deref.
Friday works terribly and I haven't tried earlier in the week but I am
usually ready to publish on Wednesdays so, yeah, there is no incentive in
trying other schedules.

Linkedin and Twitter are great places to talk about projects updates and
everything related to the "Build in public" community. I can highly
recommend getting in touch with the people posting with the tag #buildinpublic
on Twitter, and get invited to the "Build, Show and Tell" meetup, the
[website](http://buildshowandtell.com/) is not finished yet, but a great design is coming soon!


### Unrefined

Unrefined user growth is still at 1 user, 0 paying customers. The reason being
that it needs one necessary change, pointed out by many, more on that later.


## Engaging with the community

Just to repeat myself, engage with your community! Look for people living the
problem you want to solve and get in touch with them; if you are building something
it also helps follow the #buildinpublic community on Twitter, amazing people are
gathering to help each other with their own projects in an honest and transparent way.
The community is growing and this is the best time to get in touch with them (us? ;) ).

Last week it was my turn to present the project I am working on ([Unrefined](https://unrefined.one)) and I've
got the best feedback ever! The presentation and discussion was extremely smooth and
friendly and, believe me, even if you are shy you will feel like you are talking to
old time and supportive friends! Another underrated factor is that you can also
provide great feedback to other people, learn from them and grow all together, so
give yourself a favor and reach out to this amazing community!

Coming back to my personal effort to reach out to communities that could be interested
in using Unrefined, I can say that it has been totally not existent and clearly this
must be fixed in the upcoming months. Note taken&#x2026;


## Time management

During the past wave I have experienced a bit of stress, trying to do too much at the
same time, especially considering the limited time budget. What has suffered the most
was Unrefined, with almost no development or marketing dedicated to it. During the
current wave, which this post is closing, I have taken a more relaxed approach and I
think it worked; probably I am not the most productive guy on the planet but at least
I've got:

-   First, fundamental change to Unrefined to make it possible to select a different cheat sheet
-   One Clojure Bites post every two weeks; I am aiming at one per week but I have to build a queue
-   Engage with the community of builders which gave me a boost in motivation and productivity
-   Tried to stream two times; this is a time and energy consuming activity and something is telling me to try it more

My plans for the next wave is to stick to this schedule and possibly try to get some
more but it is like a stretch goal. I don't want to feel stressed for no reason and
brake a functioning routine.


## Projects


### Unrefined

This wave have seen the advent of the most requested feature, being able to select
different cheat sheets that may be more relevant to companies routines and processes.

I am currently working on an interface to enable the customization of the cheat sheet,
the second most request feature. After that I want to enable signing in, a foundational
work to give more value to prospect paying customers. This would enable providing stats
on past refinements but the value is yet to be validated.


### fpsd.codes

The goal with this website is to stick to at least one Clojure Bites every other week,
and possibly add more content; I'd like to give more updates on Unrefined or other
projects (especially in the style of small bets).

I have a backlog of changes regarding the layout structure, post grouping, enabling comments
and so on but lets see. I have some free days at the end of the month, I hope to make use
of them!

One idea in the back burner is a new series "Clojure Meals" covering bigger topics or deep
dives. Maybe Once a month? Every other month?


### Streaming

This is a new thing for me, I am learning everything from scratch and it is time consuming,
for each minute spent streaming I think it costs 30 minutes to 1 hour of preparation. The
value I can see is that it forces me to build in public and get in the flow, I'd like to have
some form of interaction but there is no audience yet LOL. I'll try it some more just to see.


## Closing words

I am still learning my way out there in the wild word of internet, I am having fun and
learning. Maybe all this effort will not be useful in the near future but I bet on the
compounding effect of all activities.

This post closes the wave 2023-05-17, see you on the next wave!

