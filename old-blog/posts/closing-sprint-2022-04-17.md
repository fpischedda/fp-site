title: Closing sprint 2022-04-17
author: FPSD
date: 2023-04-25 11:00
tags: sprint, wave, progress, numbers, analytics
---

Project name: FPSD

Project URL: <https://fpsd.codes>

Project soruces: <https://gitlab.com/fpischedda/fp-site>

Sprint merge request: <https://gitlab.com/fpischedda/fp-site/-/merge_requests/6>


## How it went

Sprint 2022-04-17 has been quite different from the first one, as I've already
figured out some details on how to run this web site I have spent more time 
on the next steps for my projects. This is a good outcome because I was worried
that I was spending too much time on marginal topics instead of making progress
on my projects.

Finally I have [released](/announcing-unrefineds-chrome-extension.html) the Chrome Extension
for [Unrefined](https://unrefined.one); I was procrastinating this release for
too long and after mentioning it in a previous post I was feeling the pressure
I've put on myself which gave the energy boots to finally make it public.  
It is not perfect yet but at least I can start to get some feedback!

## Numbers

I was trying to get to 100 unique visitor before making this post but, honestly,
having reached 92 is already a good result for something started 15 days ago,
and basically no network!

Lets have a look at what Plausible says:

![Plausible](/images/plausible-2023-04-25_10-42.png "stats")

Key takeaways:

- views are still too bounded to my post on socials, no recurring traffic
- longer posts have better performance
- (Not visible from the graph) Still no engagement
- Some sources are better than others, in order: Linkedin, Twitter, DevTo, fediverse
- Copy/Pasting the same post to all social account does not work well, especially on DevTo

## Closing words

There is still a lot to learn but I am enjoying the experience so far. Being an introvert
it makes it hard to push a bit more on the marketing side, but in the Internet
no one knows that you are a dog ;) .

One last thing, I will discontinue the term "sprint" in favor of wave; to me it feels
more in line with what I am doing and my mental processes.
