title: Announcing Unrefined's Chrome Extension
author: FPSD
date: 2023-04-24 14:15
tags: unrefined, clojure, product, chrome
---

# Project

Project name: Unrefined

Project URL: <https://unrefined.one>

Project sources: <https://gitlab.com/fpischedda/unrefined>

Extension sources: <https://gitlab.com/fpischedda/unrefined/-/tree/main/browser-extension>


## Chrome Extension for EMs is out!

After a bit of testing, the new Chrome Extension is out, targeting Unrefined's
production environment!

What was stopping its release was that I had no build script in place to package
the extension, and asking to install it by pulling the repo and loading it
manually is a bit too much, even for Engineering Managers.

It is still not an "official" extension so, to work, it must be installed using
developer mode. I am planning to put it in the Extensions Store at some point but
better to wait until it will have a least few rounds of tests.

[Here](https://developer.chrome.com/docs/extensions/mv3/getstarted/development-basics/#load-unpacked) is how to install an extension in developer mode.

You can get it from this [link](https://unrefined.one/assets/chrome-extension/unrefined.zip). It will be in Unrefined's header after the next release.


## How it works

After installing it, a user can start and manage a refinement session directly
from the extension's UI, without going to Unrefined web application.

Each time a new ticket is refined, a link to the estimation page is copied to
the clipboard so that the EM can share this link with the Engineers; actually
this is needed just the first time a refinement session is started, when
estimating new tickets, the estimation pages will update automatically, using
SSE. It may still be helpful to share the link again, in case someone else
will join an already started session, or if someone closes their browsers.

I am planning to record a couple of videos to show a session with and without
the use of the extension. (Never done that, it will be a mess for sure&#x2026;)


## Build script

A few words about the build script; I needed something extremely simple to:

-   switch to production settings
-   package everything in a zip file, for easier distribution

Another requirement for me was to try to not introduce other tools for such a
simple task.

Fortunately the web application is build using [tools.build](https://clojure.org/guides/tools_build) which is the
official, builtin, build tooling provided by Clojure, so the requirement
to not reach out to other tools has been satisfied!

It required literally few minutes to hack a target for the extension, which
does just what I need:

-   clean previous build directory if any
-   copy the extension sources to a build directory
-   switch to prod settings
-   remove unneeded files
-   create a zip archive

The [API](https://clojure.github.io/tools.build/clojure.tools.build.api.html) of tools.build library is simple and easy to reason about, plus there
are many example out there from which to get "inspiration".

Next steps, just QoL improvements for my release process:

-   build the extension when building the application's jar
-   copy new artifacts to the production environment
-   switch to the new version of the app and extension

I am still not using any containerized environment and I am postponing this step
for when there will be a real benefit; so far my small VPS is more than enough
to run this project (and a couple of others BTW).


## Reference change set

To get an idea of the changes required for the build script head over to this
[MR](https://gitlab.com/fpischedda/unrefined/-/merge_requests/43).


## Closing words

I think that the Chrome Extension is a nice quality of life improvement for EMs
using Unrefined. I am eager to get feedback on it and work on its next iterations,
there is a lot of potential there!

Remember to subscribe to the [RSS](https://fpsd.codes/rss-feed.xml) feed to get updates on my progress, and stop by
to say "hello" on my social media accounts! Yes those are not linked here yet, but
chances are that you are getting to this page from my posts ;) 

