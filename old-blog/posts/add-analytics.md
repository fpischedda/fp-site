title: Adding analytic
author: FPSD
date: 2023-04-11 15:00
tags: analytic, prep-work
---

# [2023-04-11] I need analytics

What is the point to build in public if I have no idea if my projects,
including this website, are getting traction?

I have no idea of the best practices, tools and whatever is needed to
keep track of traffic, conversion rates, retention and all the rest
so I have no clear idear to what look for, I guess I will learn along
the way.

Looking around for tools I have decided to try [plausible.io](https://plausible.io)
because it is simple and has an eye on user's privacy which is a good
bonus to have.

It offers an on premise and a hosted solution which is reasonably priced;
even if at first I was going to try the self hosted option, at 9 EUR/Month
it is not a huge investment and I can get started right away, so lets click
the "Start free trial" button and see what happens.

Another neat feature is that the dashboard can be publicly accessible, I am
considering opening them starting with the website analytics, at least it
looks intriguing.
