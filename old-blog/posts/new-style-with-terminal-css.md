title: New style using Terminal CSS
author: FPSD
date: 2023-04-22 09:15
tags: site, style, maintenance, terminal-css
---

# From 0 to 🤩

I've started this website not more than 10 days ago (at the time of this post), and  
since then it had no styling, just bare HTML, a list of posts and a link back to HOME.

While it was not a problem for me in general, at the same time I wanted to start putting  
some links here and there, for example in a nav-bar at the top.  
Instead of doing everything from scratch as I usually do, I decided to take a look around  
and from all the available options and usual suspects I've found [Terminal CSS](https://terminalcss.xyz/).

What I like about it:

- minimal styling
- easy to implement, just ispect the elements on its website and you are ready to go!
- very small size ~3k gzipped

From now on it will be my goto CSS framework, well done Terminal CSS team!
