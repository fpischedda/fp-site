(use-modules (haunt asset)
             (haunt builder blog)
             (haunt builder atom)
             (haunt builder assets)
             (haunt builder rss)
             (haunt reader)
             (haunt reader commonmark)
             (haunt site))

(setlocale LC_ALL "en_US.UTF-8")

(define (site-title-link site)
  `(a (@ (href "/")) ,(site-title site)))

(define (rss-feed-link)
  `(a (@ (href "/rss-feed.xml")) "RSS"))

(define (project-sources)
  `(a (@ (href "https://gitlab.com/fpischedda/fp-site")) "Project sources"))

(define (fpsd-default-layout site title body)
  `((doctype "html")
    (head
     (meta (@ (charset "utf-8")))
     (meta (@ (name "viewport")
	      (content "width=device-width, initial-scale=1.0")))
     (link (@ (rel "stylesheet")
	      (href "https://unpkg.com/terminal.css@0.7.2/dist/terminal.min.css")))
     (script (@ (type "text/javascript")
		(defer "true")
		(data-domain "fpsd.codes")
		(src "https://plausible.io/js/script.js")))
     (title ,(string-append title " — " (site-title site))))
    (body (@ (class "terminal"))
	  (div (@ (class "container"))
	       (section
		(div (@ (class "terminal-nav"))
		     (div (@ (class "terminal-logo"))
			  (div (@ (class "logo terminal-prompt"))
			       ,(site-title-link site)))
		     (nav (@ (class "terminal-menu"))
			  (ul (li ,(rss-feed-link))
			      (li ,(project-sources)))))))
	  (div (@ (class "container"))
	       ,body))))

(define fpsd-theme
  (theme #:name "FPSD"
         #:layout fpsd-default-layout))

(site #:title "FPSD"
      #:domain "fpsd.codes"
      #:default-metadata
      '((author . "Francesco Pischedda")
        (email  . "fp@fpsd.codes"))
      #:readers (list commonmark-reader html-reader)
      #:builders (list (blog #:theme fpsd-theme)
                       (atom-feed)
                       (atom-feeds-by-tag)
                       (rss-feed)
		       (static-directory "css")
                       (static-directory "images")))
