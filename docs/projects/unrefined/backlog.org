#+author: FPSD
#+OPTIONS: toc:nil

* Overview

This doc will keep track of the next work items for Unrefined.

Each item should have a corresponding entry (or more) in Gitlab's
issue system.

* Backlog                                                           :backlog:

** Browser extension

Goal of the browser extension is to streamline the flow of the refinement
session for both EMs and Engineers.
For EMs:

- refine a ticket from the tickets page without moving to Unrefined home page
- get links for estimations and updates
- view estimation results
- re estimate the current ticket
- estimate a new ticket

For Engineers:

- join an ongoing refinement session
- estimate current ticket from the ticket's page
- jump to the next ticket's page

The EM user flow is already implemented it needs some testing and packaging
the extension for production use. For now the extension works only on
Google Chrome/Chromium, other brosers will be addressed in future.

The Engineer user flow is yet to be implemented.

*** Priorities

**** TODO Implement a build system for the extension, package and distribute it

Breakdown of the build system:

- Generates a zip archive with the production version of the extension
- Generates a version of the extension that does not need developer mode
- Publishes the extension to the store

**** TODO Design the Engineer user flow

**** TODO Implement the Engineer user flow

**** TODO Launch and beta test

** Generic estimation cheatsheet                                    :backlog:

The estimation cheatsheet should help Engineers to breakdown the work needed
for a ticket and assign to each item the appropriate story points. The sum of
all items' estimations will give the story point for the ticket being refined.

Another benefit of the cheatsheet is that it creates consistency across estimations,
in case of outliers it is easy to find out if someone over/under estimated the
effort and makes it possible to discuss the the different views on the ticket being
refined.

Consistency also helps Product teams to forecast the effort needed to implement
new features by looking at past, similar, tickets' estimations.

Currently the cheatsheet is hardcoded and specific to a team for which this tools
has been built. Other team may need different entries and, thinking a bit further,
could be applied out of the software engineering world.

[[file:cheat-sheet-redesign.org][Details]]

*** Priorities

**** DONE Provide a second, more general, hardcoded cheatsheet

**** TODO Provide a way to customize the cheatsheet

** Organizations and users

[[file:organizations-and-users.org][Details]]

** Admin panel

** Send logs to Elasticsearch and add monitoring

** Stats

** Jira integration

- Auth with Jira account
- Store story points directly in the ticket
- Possibly run refinement sessions on the ticket page

