#+author: FPSD
#+TAGS: backlog unrefined
#+OPTIONS: toc:nil

* Updates

** What's going on

Hi there!

This sprint started in a different way, compared to the previous one.

One key difference is that I have published less content, the reason
being that I've been busy at work and I haven't been able to focus on
new posts, even if I had a couple of ideas ready:

- A bug in the guile-commonmark library used by Haunt and I have worked around it
- Work on the site layout and describe how to achieve it with Haunt
- Next steps for Unrefined

The size of the posts would have been micro, small and medium respectively, so quite
achievable compared to the previous week. So what happened? 

** Getting back to the roots

I have started this site to document the progress of my projects and I have realized
that, excluding the site itself, I haven't improved any other project so far, and
not because there is not any work to do!

To be honest, it was expected that it requires some time to bootstrap a website,
but at the same time content about the projects is king here, and I want to give
it the top priority. In order to get more content about my progresses I have to
actually work on the projects! :D

** Unrefined's backlog and next steps

Today I had a chance to get back to Unrefined, filling up its backlog and prioritizing
it based on the feedback I've received and the experience accumulated using it at
work.

Following an extract of the project's [[file:~/work/fp-site/docs/projects/unrefined/backlog.org][backlog]]:

** Browser extension

Goal of the browser extension is to streamline the flow of the refinement
session for both EMs and Engineers.
For EMs:

- refine a ticket from the tickets page without moving to Unrefined home page
- get links for estimations and updates
- view estimation results
- re estimate the current ticket
- estimate a new ticket

For Engineers:

- join an ongoing refinement session
- estimate current ticket from the ticket's page
- jump to the next ticket's page

The EM user flow is already implemented it needs some testing and packaging
the extension for production use. For now the extension works only on
Google Chrome/Chromium, other browsers will be addressed in future.

The Engineer user flow is yet to be implemented.

*** Priorities

**** TODO Implement a build system for the extension, package and distribute it

Breakdown of the build system:

- Generates a zip archive with the production version of the extension
- Generates a version of the extension that does not need developer mode
- Publishes the extension to the store

**** TODO Design the Engineer user flow

**** TODO Implement the Engineer user flow

**** TODO Launch and beta test

** Generic estimation cheat sheet

The estimation cheat sheet should help Engineers to breakdown the work needed
for a ticket and assign to each item the appropriate story points. The sum of
all items' estimations will give the story point for the ticket being refined.

Another benefit of the cheat sheet is that it creates consistency across estimations,
in case of outliers it is easy to find out if someone over/under estimated the
effort and makes it possible to discuss the the different views on the ticket being
refined.

Consistency also helps Product teams to forecast the effort needed to implement
new features by looking at past, similar, tickets' estimations.

Currently the cheat sheet is hardcoded and specific to a team for which this tools
has been built. Other team may need different entries and, thinking a bit further,
could be applied out of the software engineering world.

*** Priorities

**** TODO Provide a second, more general, hardcoded cheatsheet

**** TODO Provide a way to customize the cheat sheet

** Coming next

There are few more entries in the backlog which are not well defined yet:

- Admin panel: to at least have a view of what is going on in the web app
- Organizations and users: there is no concept of user yet, everything is publicly accessible
- Stats: once users and organizations will be in the system it might be valuable to have some stats, yet to be verified

The plan is to apply the final touches to the browsers extension and later focus on
the estimation cheat sheet which, according to the feedback, is one of the most valuable
features of the whole system, together with the estimation flow.

I am tempted to try to work on the next features during a live stream. The idea is
intriguing but at the same time terrifying, I've never streamed before so it could
be a fun experience as well as a terrible one!

** Closing words

It has been a busy week, I have produced much less content than expected but at the same
time I needed a break to think about how to structure this effort to build in public.

There are still many lessons to learn, one of which is how to use the limited time I have
at hand for my side projects. I hope to find a way to make this experience enjoyable for
everyone (me included ;) ).

Stay tuned for the next updates!


