* Goals

- Build in public
- Build brand
- Get feedback
- Get in touch with people
- Monetization

* How to

** Weekly post or every two weeks

Cadency is not well defined yet but consistency is important to build an audience.

** Share the site repo to get feedback on upcoming articles

Main idea is to really build in public, even the website.
Ideally I should collect material for the next post in org files, managed
by a public Git repo. The repository should be public in order to get feedback
on upcoming contents.
At some point the content should translate to a post.

A possible approach would be to have a branch for a specific time span, and then
merge it to master when the final content should be available in the main site.

There should be a post when a new branch is started, possibly advertising it
on social media platforms.

** Share progres on projects

Part of the contents should be about ongoing projects, this should
help me to keep focus and momentum on projects and at the same time
should help projects to be recognized.

A project is not only a software or a service but can also be a learning
path, for example studying programming languages, libraries, books or
even recipes.

* Monetization

I have no idea how this website will generate profits, but I am committed to
make it profitable.

First goal of the monetization is to measure traffic in order to possibly
attract sponsors or advertisers.

Another form of monetization is the traffic that it can generate for side
projects. This form of indirect monetization can be hard to measure but I
think it has some potenitial.

* Languages

I'll start the blog in English but I think there is potential for Italian
only posts, for example to talk and discuss about the local startup scene,
taxes, or other italian specific topics that may no be interesting to non
italian readers.
