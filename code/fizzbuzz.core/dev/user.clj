(ns user
  (:require [clojure.repl.deps :refer [add-lib]]))

(comment
  (add-lib 'org.clojure/core.match)
  (require '[clojure.core.match :refer [match]])
  )
