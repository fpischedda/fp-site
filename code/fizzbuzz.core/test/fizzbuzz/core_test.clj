(ns fizzbuzz.core-test
  (:require [clojure.test :refer [deftest are]]
            [fizzbuzz.core :as sut]))

(deftest OMG-FizzBuzz
  (are [argument expected] (= expected (sut/fizz-buzz argument))
    1 1
    2 2
    3 "Fizz"
    6 "Fizz"
    5 "Buzz"
    10 "Buzz"
    15 "FizzBuzz"))
