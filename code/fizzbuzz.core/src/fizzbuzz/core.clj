(ns fizzbuzz.core
  (:require [clojure.core.match :refer [match]]))

(defn fizz-buzz
  "Implements FizzBuzz, returning
- The string Fizz if the number is divisible by 3
- The string Buzz if the number is divisible by 5
- The string FizzBuzz if the number is divisible by both 3 and 5
- The argument if none of the previous conditions are met
"
  [n]
  (match [(mod n 3) (mod n 5)]
         [0 0] "FizzBuzz"
         [0 _] "Fizz"
         [_ 0] "Buzz"
         :else n))

(comment 
  (require '[clojure.repl.deps :refer [add-lib]])
  (add-lib 'org.clojure/core.match)

  (for [n (range 1 17)]
    (fizz-buzz n))
  )
